//
//  PostCard.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 22/03/21.
//

import Foundation
import UIKit

struct PostCard {
    static var details: PostCard = PostCard()
    var type: String?
    var image: UIImage?
    var frontText: String?
    var backText: String?
    var date: Date?
    var uuid: UUID?

}


