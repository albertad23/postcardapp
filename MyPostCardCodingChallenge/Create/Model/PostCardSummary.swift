//
//  PostCardSummary.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import Foundation
import RxSwift
import RxCocoa

class PostCardSummary {
    static let sharedPostCard = PostCardSummary()
    var postCard: BehaviorRelay<[PostCard]> = BehaviorRelay(value: [])
}
