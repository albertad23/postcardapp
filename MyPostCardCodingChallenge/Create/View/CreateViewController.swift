//
//  CreateViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 21/03/21.
//

import UIKit

enum PostCardType: String {
    case Regular = "regular"
    case Foldable = "foldable"
}

class CreateViewController: UIViewController {

    @IBOutlet weak var regularPostCardButton: UIButton! {
        didSet {
            regularPostCardButton.setTitle("Regular PostCard", for: .normal)
        }
    }

    @IBOutlet weak var foldablePostCardButton: UIButton! {
        didSet {
            foldablePostCardButton.setTitle("Foldable PostCard", for: .normal)
        }
    }

    @IBAction func regularPostCardButton(_ sender: Any) {
        PostCard.details.type = PostCardType.Regular.rawValue
        goToNextTab()
    }
    @IBAction func foldablePostCardButton(_ sender: Any) {
        PostCard.details.type = PostCardType.Foldable.rawValue
        goToNextTab()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItem.isEnabled = true
    }

    @objc func back(sender: UIBarButtonItem) {
        if let tabBar = tabBarController {
            tabBarController?.selectedIndex = tabBar.selectedIndex + 1
            print("halo")
        }
    }

    func goToNextTab() {
        if let tabBar = tabBarController {
            tabBarController?.selectedIndex = tabBar.selectedIndex + 1

        }
    }
}
