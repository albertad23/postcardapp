//
//  AddTextViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 22/03/21.
//

import UIKit
import RxSwift
import RxCocoa

class AddTextViewController: UIViewController {

    // MARK - Outlet
    @IBOutlet weak var backTextField: UITextField! {
        didSet {
            backTextField.placeholder = "Back Text"
        }
    }
    @IBOutlet weak var frontTextField: UITextField! {
        didSet {
            frontTextField.placeholder = "Front Text"
        }
    }
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.setTitle("Next", for: .normal)
            nextButton.isEnabled = false
        }
    }

    // MARK - Variables
    private let disposeBag = DisposeBag()


    // MARK - IBActions
    @IBAction func nextButton(_ sender: Any) {
        if let tabBar = tabBarController {
            PostCard.details.frontText = frontTextField.text ?? ""
            PostCard.details.backText = backTextField.text ?? ""
            tabBarController?.selectedIndex = tabBar.selectedIndex + 1
        }

        print(wordCount(word: backTextField.text ?? ""))
    }

    // MARK - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareTextFields()
    }



    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let frontText = PostCard.details.frontText {
            frontTextField.text = frontText
        }
        if let backText = PostCard.details.backText {
            backTextField.text = backText
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItem.isEnabled = true
        backTextField.isHidden = PostCard.details.type == PostCardType.Regular.rawValue
    }

    // MARK - Functions
    func prepareTextFields()  {
        frontTextField.delegate = self
        backTextField.delegate = self
        
        let frontTextValidation = frontTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)

        let backTextValidation = backTextField
            .rx.text
            .map({!($0?.isEmpty ?? true)})
            .share(replay: 1)

        if PostCard.details.type == PostCardType.Regular.rawValue {
            frontTextValidation.bind(to: nextButton.rx.isEnabled).disposed(by: disposeBag)
            return
        }


        let enableButton = Observable.combineLatest(frontTextValidation, backTextValidation) { (login, name) in
           return login && name
        }

        enableButton.bind(to: nextButton.rx.isEnabled).disposed(by: disposeBag)

    }

    func wordCount(word: String) -> Int {
        let words = word.components(separatedBy: .whitespacesAndNewlines)
        let filteredWords = words.filter({
            (word) -> Bool in
                word != ""
        })
        let wordCount = filteredWords.count
        return wordCount
    }
}

extension AddTextViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        if textField == frontTextField {
            let currentText = textField.text ?? ""
            guard let stringRange = Range(range, in: currentText) else { return false }

            let updatedText = currentText.replacingCharacters(in: stringRange, with: string)
            return updatedText.count <= 30
        }
        if textField == backTextField {
            let updatedText = wordCount(word: textField.text ?? "")
            return updatedText < 4
        }
        return false
    }
}
