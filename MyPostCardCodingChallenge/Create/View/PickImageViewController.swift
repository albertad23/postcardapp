//
//  PickImageViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 22/03/21.
//

import UIKit
import RxSwift
import RxCocoa

class PickImageViewController: UIViewController {

    // MARK - Outlets
    @IBOutlet weak var imageView: UIImageView!

    @IBOutlet weak var addButton: UIButton! {
        didSet {
            addButton.setTitle("Add Image", for: .normal)
        }
    }

    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.setTitle("Next", for: .normal)
            nextButton.isEnabled = false
        }
    }

    // MARK - Variables
    var imagePicker = UIImagePickerController()
    private let disposeBag = DisposeBag()

    // MARK - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        if let image = PostCard.details.image {
            imageView.image = image
        }

        prepareImage()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItem.isEnabled = true
    }

    func prepareImage()  {
        imageView.rx.isEmpty.subscribe(onNext: { isEmpty in
            self.nextButton.isEnabled = !isEmpty
        }).disposed(by: disposeBag)
    }

    // MARK - IBActions
    @IBAction func addButton(_ sender: Any) {

        let alert = UIAlertController(title: "Title", message: "Please Select an Option", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (_) in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (_) in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
            alert.dismiss(animated: true)
         }))

        self.present(alert, animated: true, completion: {
            print("completion block")
        })

    }

    @IBAction func nextButton(_ sender: Any) {
        if let tabBar = tabBarController {
            tabBarController?.selectedIndex = tabBar.selectedIndex + 1
        }
    }

    // MARK - Functions
    func openCamera() {
        if UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)

        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary() {
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true, completion: nil)
    }
}

extension PickImageViewController: UINavigationControllerDelegate {

}

extension PickImageViewController: UIImagePickerControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.editedImage] as? UIImage else {
            return
        }

        imageView.image = image
        PostCard.details.image = image
    }
}
