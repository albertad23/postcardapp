//
//  TabbarViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 24/03/21.
//

import UIKit
import RxSwift
import RxCocoa


class TabbarViewController: UITabBarController {

    private let disposeBag = DisposeBag()
    private var canOpenTabBar: Bool = false

    override func viewDidLoad() {
        self.delegate = self
    }
}

extension TabbarViewController: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        if viewController == tabBarController.viewControllers?[1] {
            return (PostCard.details.type != nil)
        }
        if viewController == tabBarController.viewControllers?[2] {
            return (PostCard.details.image != nil)
        }

        if viewController == tabBarController.viewControllers?[3] || viewController == tabBarController.viewControllers?[4] {
            guard let frontText = PostCard.details.frontText else {
                return false
            }
            guard let backText = PostCard.details.backText else {
                return false
            }

            if PostCard.details.type == PostCardType.Regular.rawValue {
                return ((!frontText.isEmpty))
            } else {
                return ((!frontText.isEmpty && !backText.isEmpty))
            }
        }
        else {
            return true
        }
    }
}

