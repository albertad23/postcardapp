//
//  PreviewViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import UIKit


class PreviewViewController: UIViewController {

    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.setTitle("Next", for: .normal)
        }
    }
    @IBAction func nextButton(_ sender: Any) {
        if let tabBar = tabBarController {
            tabBarController?.selectedIndex = tabBar.selectedIndex + 1
        }
    }
    
    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            imageView.image = PostCard.details.image
        }
    }

    @IBOutlet weak var frontTextLabel: UILabel! {
        didSet {
            frontTextLabel.text = "Front Text: \(PostCard.details.frontText ?? "")"
        }
    }

    @IBOutlet weak var backTextLabel: UILabel! {
        didSet {
            backTextLabel.text = "Back Text: \(PostCard.details.backText ?? "")"
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        imageView.image = PostCard.details.image
        frontTextLabel.text = "Front Text: \(PostCard.details.frontText ?? "")"
        backTextLabel.text = "Back Text: \(PostCard.details.backText ?? "")"
        tabBarItem.isEnabled = true
    }
}
