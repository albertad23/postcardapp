//
//  SummaryViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import UIKit
import CoreData

class SummaryViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var nextButton: UIButton! {
        didSet {
            nextButton.setTitle("Next", for: .normal)
        }
    }
    
    var viewModel: SummaryViewModel = SummaryViewModel()
    
    @IBAction func saveButton(_ sender: Any) {
        viewModel.update(postCard: PostCard.details)
        navigationController?.popViewController(animated: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarItem.isEnabled = true
        imageView.image = PostCard.details.image
        typeLabel.text = "Type: \(PostCard.details.type ?? "")"
    }
}

