//
//  SummaryViewModel.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import Foundation
import UIKit
import CoreData

class SummaryViewModel {

    func save(postCard: PostCard) {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
      }

        let managedContext = appDelegate.persistentContainer.viewContext

        guard let entity = NSEntityDescription.entity(forEntityName: "ArchiveDataModel",
                                                      in: managedContext) else {
            return
        }

        let postCardObject = NSManagedObject(entity: entity,
                                   insertInto: managedContext)

        postCardObject.setValue(postCard.backText, forKeyPath: "backText")
        postCardObject.setValue(postCard.frontText, forKeyPath: "frontText")
        postCardObject.setValue(Date(), forKeyPath: "date")
        postCardObject.setValue(postCard.image?.pngData(), forKeyPath: "image")
        postCardObject.setValue(postCard.type, forKeyPath: "type")
        postCardObject.setValue(UUID(), forKey: "uuid")

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }

    func update(postCard: PostCard){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "ArchiveDataModel")
        guard let uuid = postCard.uuid else {
            save(postCard: postCard)
            return
        }
        fetchRequest.predicate = NSPredicate(format: "uuid = %@", uuid as CVarArg)
        do{
            let fetch = try managedContext.fetch(fetchRequest)
            guard let dataToUpdate = fetch[0] as? NSManagedObject else {
                return
            }
            dataToUpdate.setValue(postCard.backText, forKeyPath: "backText")
            dataToUpdate.setValue(postCard.frontText, forKeyPath: "frontText")
            dataToUpdate.setValue(Date(), forKeyPath: "date")
            dataToUpdate.setValue(postCard.type, forKey: "type")

            try managedContext.save()
        }
        catch let err{
            print(err)
        }
    }
}
