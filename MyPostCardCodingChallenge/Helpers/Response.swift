//
//  Response.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 24/03/21.
//

import Foundation

final class Response: Codable {
    var success: Bool
    var uid: String
    var name: String
    var lastname: String
    var email: String
    var balance: String
    var currencyiso: String
    var newsletter: Bool
    var optIn: Bool
    var friendcode: String
    var friendcode_revenue: String
    var registerdate: String
    var userAddressbookLink: String
    var birthday_reminder: Int


}
