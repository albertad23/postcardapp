//
//  NetworkManager.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 21/03/21.
//

import Foundation
import Alamofire

class NetworkManager {

    let baseURL = "https://www.mypostcard.com/"

    func login (_ credentials: Credentials, completion: @escaping(Result<Response, Error>) -> Void){

        do{
            guard let url = URL(string: baseURL+"mobile/login.php?json") else {
                return
            }
            var urlRequest = URLRequest(url: url)
            urlRequest.httpMethod = "POST"

            let body = HTTPUtils.formUrlencode([
                "email": credentials.email,
                "password": credentials.password
            ])

            urlRequest.httpBody = body.data(using: .utf8)


            let dataTask = URLSession.shared.dataTask(with: urlRequest){ data, response, _ in

                guard let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode == 200, let jsonData = data else {
                    completion(.failure(APIError.responseProblem))
                    return
                }

                do {
                    let messageData = try JSONDecoder().decode(Response.self, from: jsonData)
                    completion(.success(messageData))
                }
                catch {
                    completion(.failure(error))
                    print("error: \(error)")
                }
            }
            dataTask.resume()
        }

    }
}

class HTTPUtils {
    public class func formUrlencode(_ values: [String: String]) -> String {
        return values.map { key, value in
            return "\(key.formUrlencoded())=\(value.formUrlencoded())"
        }.joined(separator: "&")
    }
}

extension String {
    static let formUrlencodedAllowedCharacters =
        CharacterSet(charactersIn: "0123456789" +
            "abcdefghijklmnopqrstuvwxyz" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "-._* ")

    public func formUrlencoded() -> String {
        let encoded = addingPercentEncoding(withAllowedCharacters: String.formUrlencodedAllowedCharacters)
        return encoded?.replacingOccurrences(of: " ", with: "+") ?? ""
    }
}

enum APIError: Error {
    case responseProblem
}
