//
//  Credentials.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 24/03/21.
//

import Foundation

final class Credentials : Codable{
    var email: String
    var password: String

    init(email: String, password: String){
        self.email = email
        self.password = password
    }
}
