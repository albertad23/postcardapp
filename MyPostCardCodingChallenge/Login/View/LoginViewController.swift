//
//  ViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 21/03/21.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {

    // MARK: - Variables
    var viewModel: LoginViewModel = LoginViewModel()

    // MARK: - Outlets
    @IBOutlet weak var userNameTextField: UITextField! {
        didSet {
            userNameTextField.placeholder = "User Name"
        }
    }
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.placeholder = "Password"
        }
    }
    @IBOutlet weak var submitButton: UIButton!

    // MARK: - IBActions
    @IBAction func submitButton(_ sender: Any) {
        if let userName = userNameTextField.text, let password = passwordTextField.text {
            viewModel.didTapSigninButton(with: userName, password: password) { result in
                switch result {
                case .success( _):
                    DispatchQueue.main.async {
                    let storyboard = UIStoryboard(name: "MainViewController", bundle: nil)
                       let mainTabBarController = storyboard.instantiateViewController(identifier: "MainNavigationController")
                       (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(mainTabBarController)
                    }
                case .failure( _):
                    DispatchQueue.main.async {

                    let alert = UIAlertController(title: "Loading Failed", message: "The loading process is failed. Please try again letter", preferredStyle: UIAlertController.Style.alert)

                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
                    }))

                    self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }

    }
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigation()
        setupView()
    }

    // MARK: - Functions
    func setupNavigation() {
        navigationItem.title = "Login"
    }

    func setupView() {
        userNameTextField.delegate = self
        passwordTextField.delegate = self
        submitButton.isEnabled = false
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if
            let username = userNameTextField.text, !username.isEmpty,
            let password = passwordTextField.text, !password.isEmpty {
            submitButton.isEnabled = true
            return

        }
        submitButton.isEnabled = false
    }

}



