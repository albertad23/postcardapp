//
//  LoginViewModel.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 21/03/21.
//

import Foundation
import UIKit

class LoginViewModel {

    func didTapSigninButton(with userName: String, password: String, completion: @escaping(Result<Response, Error>) -> Void ) {
        let networkManager = NetworkManager()
        let credentials = Credentials(email: userName, password: password)
        networkManager.login(credentials) { result in
            switch result {
            case .failure(let error):
                print(error)
                completion(.failure(error))
            case .success(let response):
                completion(.success(response))
            }
        }
    }
}

