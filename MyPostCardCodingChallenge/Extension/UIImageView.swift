//
//  File.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 25/03/21.
//


import RxSwift
import RxCocoa

extension Reactive where Base: UIImageView {
    var isEmpty: Observable<Bool> {
        return observe(UIImage.self, "image").map{ $0 == nil }
    }
}
