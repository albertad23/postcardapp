//
//  MainViewModel.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 22/03/21.
//

import Foundation
import UIKit
import CoreData
import RxSwift
import RxCocoa

class MainViewModel {

    func getPostCards(completion: @escaping () -> ()){

        var postCards = [PostCard]()

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext

        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ArchiveDataModel")

        do {
            guard let result = try managedContext.fetch(fetchRequest) as? [NSManagedObject] else {
                return
            }
            result.forEach{ item in
                if let imageData = item.value(forKey: "image") as? Data {
                    postCards.append(
                        PostCard(type: item.value(forKey: "type") as? String, image: UIImage(data: imageData, scale: 1), frontText: item.value(forKey: "frontText") as? String, backText: item.value(forKey: "backText") as? String, date: item.value(forKey: "date") as? Date, uuid: item.value(forKey: "uuid") as? UUID))
                }
                else {
                    postCards.append(
                        PostCard(type: item.value(forKey: "type") as? String, image: nil, frontText: item.value(forKey: "frontText") as? String, backText: item.value(forKey: "backText") as? String, date: item.value(forKey: "date") as? Date, uuid: item.value(forKey: "uuid") as? UUID)
                    )
                }
            }
            postCards = postCards.sorted(by: { $1.date ?? Date() < $0.date ?? Date() })
        }
        catch let err{
            print(err)
        }

        PostCardSummary.sharedPostCard.postCard = BehaviorRelay(value: postCards)

        completion()
    }
}
