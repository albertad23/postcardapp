//
//  MainViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 21/03/21.
//

import UIKit
import RxSwift

class MainViewController: UIViewController {


    // MARK: - Variables
    var viewModel: MainViewModel = MainViewModel()
    private let disposeBag = DisposeBag()

    // MARK: - Outlets
    @IBOutlet weak var createButton: UIButton! {
        didSet {
            createButton.setTitle("Create", for: .normal)
        }
    }
    @IBOutlet weak var archiveButton: UIButton! {
        didSet {
            archiveButton.setTitle("Archive \((PostCardSummary.sharedPostCard.postCard.value.count))", for: .normal)
        }
    }

    // MARK: - Actions
    @IBAction func createButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "CreateViewController", bundle: nil)
        guard let tabbar = storyboard.instantiateViewController(withIdentifier: "CreateTabBarController") as? UITabBarController else {
            return
        }
        PostCard.details = PostCard()
        navigationController?.pushViewController(tabbar, animated: true)
    }
    
    @IBAction func archiveButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ArchiveViewController", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "ArchiveViewController")
        navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK: - Lifecycles
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getPostCards {
            self.setupCartObserver()
        }
    }


    // MARK: - Functions
    func setupCartObserver() {
        PostCardSummary.sharedPostCard.postCard.asObservable().subscribe(onNext: {
            [unowned self] postCard in
            self.archiveButton.setTitle("Archive \((PostCardSummary.sharedPostCard.postCard.value.count))", for: .normal)
        }).disposed(by: disposeBag)
    }

}
