//
//  ArchiveViewModel.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import Foundation
import UIKit
import CoreData

class ArchiveViewModel {

    // MARK: - Variables
    var postCards: [PostCard] = PostCardSummary.sharedPostCard.postCard.value
    var filteredPostCards: [PostCard] = PostCardSummary.sharedPostCard.postCard.value
    var heightForRow: Float = 120

    // MARK: - Functions
    func segmentedControlDidChange(with value: Int, complation: @escaping() -> ()) {
        switch value {
        case 0: filteredPostCards = postCards
        case 1: filteredPostCards = postCards.filter({$0.type == PostCardType.Regular.rawValue})
        case 2: filteredPostCards = postCards.filter({$0.type == PostCardType.Foldable.rawValue})
        default: filteredPostCards = postCards
        }
        complation()
    }

    func deletePostCard(_ uuid:UUID, indexPath: IndexPath, completion: @escaping () -> () ){

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }

        let managedContext = appDelegate.persistentContainer.viewContext

        let fetchRequest:NSFetchRequest<NSFetchRequestResult> = NSFetchRequest.init(entityName: "ArchiveDataModel")
        fetchRequest.predicate = NSPredicate(format: "uuid = %@", uuid as CVarArg)

        do{
            guard let dataToDelete = try managedContext.fetch(fetchRequest)[0] as? NSManagedObject else {
                return
            }
            managedContext.delete(dataToDelete)
            postCards.remove(at: indexPath.row)
            filteredPostCards.remove(at: indexPath.row)
            try managedContext.save()

        } catch let err{
            print(err)
        }
        completion()
    }
}
