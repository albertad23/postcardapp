//
//  ArchiveTableViewCell.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 23/03/21.
//

import UIKit

class ArchiveTableViewCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var archiveImageView: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!

    @IBOutlet weak var frontTextLabel: UILabel!

    @IBOutlet weak var backTextLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    // MARK: - Functions
    func setupCell(with postCard: PostCard) {

        if let date = postCard.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd/MM/yyyy"
            let stringDate = dateFormatter.string(from: date)
            dateLabel.text = stringDate
        }
        typeLabel.text = postCard.type
        frontTextLabel.text = postCard.frontText
        backTextLabel.text = postCard.backText
        archiveImageView.image = postCard.image
    }
}
