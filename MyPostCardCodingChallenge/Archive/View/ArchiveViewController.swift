//
//  ArchiveViewController.swift
//  MyPostCardCodingChallenge
//
//  Created by Albert Adisaputra on 22/03/21.
//

import UIKit
import RxSwift
import CoreData

class ArchiveViewController: UIViewController  {
    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Variables
    let archiveCellIdentifier = "ArchiveTableViewCell"
    var viewModel: ArchiveViewModel = ArchiveViewModel()
    private let disposeBag = DisposeBag()


    // MARK: - Lifescycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: archiveCellIdentifier, bundle: nil), forCellReuseIdentifier: archiveCellIdentifier)
        tableView.tableFooterView = UIView()
        Observable.of(PostCardSummary.sharedPostCard.postCard).subscribe(onNext: {
            [unowned self] _ in
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
        }).disposed(by: disposeBag)
        PostCardSummary.sharedPostCard.postCard.subscribe(onNext: {
            [unowned self] _ in
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
        }).disposed(by: disposeBag)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }


    // MARK: - IBAction
    @IBAction func didChangeSegment(_ sender: UISegmentedControl) {
        viewModel.segmentedControlDidChange(with: sender.selectedSegmentIndex) {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    // MARK: - Functions
    func showAlert() {
        let alert = UIAlertController(title: "Loading Failed", message: "The loading process is failed. Please try again letter", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { _ in
        }))

        self.present(alert, animated: true, completion: nil)
    }

    func loadPostCard(with indexPath: IndexPath) {
        if indexPath.row % 6 == 1 {
            showAlert()
        }
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "CreateViewController", bundle: nil)
        let vc = mainStoryboard.instantiateViewController(withIdentifier: "CreateTabBarController")
        navigationController?.pushViewController(vc, animated: true)
        PostCard.details = viewModel.postCards[indexPath.row]
    }

    func syncPostCard() {
        let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default
    ))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: - TableView DataSource
extension ArchiveViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.filteredPostCards.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: archiveCellIdentifier, for: indexPath) as? ArchiveTableViewCell else {
            return UITableViewCell()
        }
        cell.setupCell(with: viewModel.filteredPostCards[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let alert = UIAlertController(title: "Title", message: "Please Select an Option", preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Load", style: .default, handler: { (_) in
            self.loadPostCard(with: indexPath)
        }))

        if indexPath.row % 3 == 2 {
            alert.addAction(UIAlertAction(title: "Sync", style: .default, handler: { (_) in
                self.syncPostCard()
            }))
        }

        alert.addAction(UIAlertAction(title: "Delete", style: .default, handler: { (_) in
            guard let uuid = self.viewModel.filteredPostCards[indexPath.row].uuid else {
                return
            }
            self.viewModel.deletePostCard(uuid, indexPath: indexPath) {
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }

        }))

        alert.addAction(UIAlertAction(title: "Dismiss", style: .cancel, handler: { (_) in
            alert.dismiss(animated: true)
         }))

        self.present(alert, animated: true, completion: {
            print("completion block")
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        })
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


// MARK: - TableView Delegate
extension ArchiveViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.heightForRow)
    }
}

